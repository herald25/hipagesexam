# README #

Practical exam for hipages.

### Libraries ###

* RxJava2
* Dagger2
* RxAndroid
* RxKotlin
* Retrofit 2
* Glide 4
* Epoxy
* MVVM
* LiveData
* KotlinX
* Timber for logging

### Exam Requirements ###

* The app should include a toolbar and not an actionbar.
* The app should be backwards compatible to api level 16
* It is expected the UI will be done within a fragment.
* Items that can be clicked should have the correct selection state as per the os version
* Do not spend more than 2-3 hours maximum

### Architecture ###

* I used MVVM and LiveData for this repository also I integrate semi-clean architecture by using gateway
