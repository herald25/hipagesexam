package com.sample.hipagesexam.common.retrofit.response

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SuccessfullyDto(
    @Optional
    @SerialName("id")
    var id: String? = null
)