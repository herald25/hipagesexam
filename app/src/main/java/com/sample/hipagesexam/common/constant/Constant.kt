package com.sample.hipagesexam.common.constant

class Constant {

    companion object {
        const val EMPTY = "-"
    }

    class ErrorMessage {
        companion object {
            const val ERROR_NO_INTERNET_CONNECTION = "No Internet Connection"
            const val ERROR_INVALID_TOKEN = "Invalid Access Token"
            const val ERROR_REQUEST_TIMEOUT = "Request timeout."
            const val ERROR_SOMETHING_WENT_WRONG = "Something went wrong."
        }
    }

    class ApiStatus {
        companion object {
            const val STATUS_SUCCESS = 0
            const val STATUS_FAILED = 9
        }
    }

}