package com.sample.hipagesexam.common.retrofit.executor

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response

interface ResponseProvider {
    fun <T> executeResponseSingle(response: Response<T>): Single<T>
    fun <T> executeResponseCompletable(response: Response<T>): Completable
}