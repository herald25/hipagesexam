package com.sample.hipagesexam.common.retrofit.executor

import io.reactivex.Scheduler

interface SchedulerProvider {
    fun ui(): Scheduler
    fun io(): Scheduler
    fun newThread(): Scheduler
    fun computation(): Scheduler
}