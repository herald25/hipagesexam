package com.sample.hipagesexam.common.retrofit.executor

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import timber.log.Timber


class ResponseProviderImpl : ResponseProvider {

    override fun <T> executeResponseSingle(response: Response<T>): Single<T> {
        return if (response.isSuccessful) {
            Timber.d("isSuccessful")
            Single.just(response.body())
        } else {
            Timber.d("!isSuccessful")
            //val errorResponse = response.errorBody()!!.string()
            Single.error(Exception(response.message()))
        }
    }

    override fun <T> executeResponseCompletable(response: Response<T>): Completable {
        return if (response.isSuccessful) {
            Timber.d("isSuccessful")
            Completable.complete()
        } else {
            Timber.d("!isSuccessful")
            //val errorResponse = response.errorBody()!!.string()
            Completable.error(Exception(response.message()))
        }
    }


}