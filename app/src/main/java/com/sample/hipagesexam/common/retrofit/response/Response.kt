package com.sample.hipagesexam.common.retrofit.response

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Response<T>(
    @Optional
    @SerialName("status")
    var status: Int = 9,
    @Optional
    @SerialName("data")
    var data: T? = null,
    @Optional
    @SerialName("message")
    var message: String? = null
)