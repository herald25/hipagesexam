package com.sample.hipagesexam.app.injection.module

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.widget.LinearLayoutManager
import com.sample.hipagesexam.app.common.connectivity.ConnectivityChecker
import com.sample.hipagesexam.app.common.manager.CacheManager
import com.sample.hipagesexam.app.common.navigation.Navigator
import com.sample.hipagesexam.app.util.ViewUtil
import com.sample.hipagesexam.common.retrofit.executor.ResponseProvider
import com.sample.hipagesexam.common.retrofit.executor.ResponseProviderImpl
import com.sample.hipagesexam.common.retrofit.executor.SchedulerProvider
import com.sample.hipagesexam.common.retrofit.executor.SchedulerProviderImpl
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    fun context(application: Application): Context = application

    @Singleton
    @Provides
    internal fun compositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Singleton
    @Provides
    internal fun schedulerProvider(): SchedulerProvider {
        return SchedulerProviderImpl()
    }

    @Singleton
    @Provides
    internal fun responseProvider(): ResponseProvider {
        return ResponseProviderImpl()
    }

    @Singleton
    @Provides
    internal fun linearLayoutManager(context: Context): LinearLayoutManager {
        return LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    @Singleton
    @Provides
    internal fun navigator(): Navigator {
        return Navigator()
    }


    @Singleton
    @Provides
    internal fun connectivityChecker(context: Context): ConnectivityChecker {
        return ConnectivityChecker(
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        )
    }

    @Singleton
    @Provides
    internal fun viewUtil(context: Context): ViewUtil {
        return ViewUtil(context)
    }

    @Singleton
    @Provides
    internal fun cacheManager(): CacheManager {
        return CacheManager()
    }


}