package com.sample.hipagesexam.app.common.manager

class CacheManager {

    private val mCache: MutableMap<String, String> = mutableMapOf()

    init {
        /*
        *  every time init is called increment instance count
        *  just in case somehow we break singleton rule, this will be
        *  called more than once and myInstancesCount > 1 == true
        */
        ++myInstancesCount
    }


    companion object {
        //Debuggable field to check instance count

        var myInstancesCount = 0
        private val mInstance: CacheManager =
            CacheManager()

        @Synchronized
        fun getInstance(): CacheManager {
            return mInstance
        }
    }

    /*
     * retrieves a boolean value for a corresponding key
     * if this key is previously not inserted using put(..., ...) above
     * it will return null
     */
    fun getBoolean(key: String): Boolean {
        return get(key) == "1"
    }

    /*
    * Put a key and corresponding value in a map of String, Boolean
    */
    fun putBoolean(key: String, value: Boolean) {
        put(key, if (value) "1" else "0")
    }

    /*
     * Put a key and corresponding value in a map of String, String
     */
    fun put(key: String, value: String) {
        when {
            !key.isEmpty() -> mCache[key] = value
            else -> throw IllegalArgumentException("Key cannot be empty")
        }
    }

    /*
     * retrieves a string value for a corresponding key
     * if this key is previously not inserted using put(..., ...) above
     * it will return null
     */
    fun get(key: String): String? {
        return when {
            mCache.containsKey(key) -> mCache[key]
            else -> null
        }
    }

    /*
       * it will clear specific cache
       */
    fun clear(key: String) {
        mCache.remove(key)
    }

    /*
     * it will clear cache
     */
    fun clear() {
        mCache.clear()
    }
}