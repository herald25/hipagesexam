package com.sample.hipagesexam.app.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.AppCompatImageView
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.sample.hipagesexam.R
import com.sample.hipagesexam.common.constant.Constant
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class ViewUtil(private val mContext: Context) {

    companion object {
        const val DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        const val DATE_FORMAT_ISO_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        const val DATE_FORMAT_ISO_WITHOUT_T = "yyyy-MM-dd HH:mm:ss"
        const val DATE_FORMAT_ISO_DATE = "yyyy-MM-dd"
        const val DATE_FORMAT_DEFAULT = "MMMM dd, yyyy, h:mm a"
        const val DATE_FORMAT_DATE = "MMMM dd, yyyy"
        const val DATE_FORMAT_DATE_SLASH = "MM/dd/yyyy"
        const val DATE_FORMAT_DATE_SLASH_DAY = "dd/MM/yyyy"
        const val DATE_FORMAT_WITHOUT_TIME = "MMMM dd, yyyy, EEEE"
        const val DATE_FORMAT_TIME = "h:mm a"

        const val REGEX_FORMAT_HAS_NUMBER = ".*\\d+.*"
        const val REGEX_FORMAT_HAS_ALPHA = ".*[a-z].*"
        const val REGEX_FORMAT_HAS_SYMBOL = ".*[^A-Za-z0-9].*"
    }


    private val formatStrings =
        Arrays.asList(DATE_FORMAT_ISO, DATE_FORMAT_ISO_WITHOUT_T, DATE_FORMAT_ISO_DATE)

    fun getUdId(activity: Activity) =
        Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID)

    fun dismissKeyboard(activity: Activity) {
        val focus = activity.currentFocus
        if (focus != null) {
            val mContext = activity.applicationContext
            val inputMethodManager = mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(focus.windowToken, 0)
        }
    }

    fun isShownKeyboard(TAG: String, contentView: View): Boolean? {
        val isShown = booleanArrayOf(false)
        contentView.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            contentView.getWindowVisibleDisplayFrame(r)
            val screenHeight = contentView.rootView.height

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            val keypadHeight = screenHeight - r.bottom

            Log.d(TAG, "keypadHeight = $keypadHeight")

            isShown[0] = keypadHeight > screenHeight * 0.15
        }
        return isShown[0]
    }

    fun isSoftKeyboardShown(view: View?): Boolean {
        val r = Rect()
        // r will be populated with the coordinates of your view that area still
        // visible.
        var keypadHeight = 0
        var screenHeight = 0
        //r will be populated with the coordinates of your view that area still visible.
        if (view != null) {
            view.getWindowVisibleDisplayFrame(r)
            screenHeight = view.rootView.height
            keypadHeight = screenHeight - r.bottom
        }
        return keypadHeight > screenHeight * 0.15
    }

    //    public void setErrorValidation(Boolean isValid, EditText editText) {
    //        if (isValid) {
    //            Drawable checkDrawable = ContextCompat.getDrawable(mContext, R.drawable.ic_check_grey_24dp);
    //            checkDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
    //            editText.setCompoundDrawablesWithIntrinsicBounds(null, null, checkDrawable, null);
    //            editText.setCompoundDrawablePadding(20);
    //        } else {
    //            Drawable closeDrawable = ContextCompat.getDrawable(mContext, R.drawable.ic_close_grey_24dp);
    //            closeDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.colorColorRed), PorterDuff.Mode.SRC_ATOP);
    //            editText.setCompoundDrawablesWithIntrinsicBounds(null, null, closeDrawable, null);
    //            editText.setCompoundDrawablePadding(20);
    //            if (getTextInputLayout(editText).getHint().toString().equals(mContext.getString(R.string.hint_contactno))) {
    //                setError(editText, mContext.getString(R.string.error_mobile_number_already_exist));
    //            } else if (getTextInputLayout(editText).getHint().toString().equals(mContext.getString(R.string.hint_username))) {
    //                setError(editText, mContext.getString(R.string.error_username_already_exist));
    //            } else {
    //                setError(editText, mContext.getString(R.string.error_email_already_exist));
    //            }
    //        }
    //    }

    fun makeLinks(textView: TextView, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        val spannableString = SpannableString(textView.text)
        for (i in links.indices) {
            val clickableSpan = clickableSpans[i]
            val link = links[i]

            val startIndexOfLink = textView.text.toString().indexOf(link)
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spannableString.setSpan(
                UnderlineSpan(),
                startIndexOfLink,
                startIndexOfLink + link.length,
                0
            )
        }
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    fun getTextInputLayout(editText: View): TextInputLayout? {
        var currentView = editText
        for (i in 0..1) {
            val parent = currentView.parent
            if (parent is TextInputLayout) {
                return parent
            } else {
                currentView = parent as View
            }
        }
        return null
    }

    fun setEditTextMaxLength(editText: EditText?, length: Int) {
        val filterArray = arrayOfNulls<InputFilter>(1)
        filterArray[0] = InputFilter.LengthFilter(length)
        editText?.filters = filterArray
    }

    fun setError(editText: EditText, message: String) {
        val editTextParent = getTextInputLayout(editText)
        if (editTextParent != null) {
            editTextParent.error = message
            editTextParent.isErrorEnabled = true
        }
    }

    fun setErrorField(editText: EditText, message: String) {
        if (editText.text.toString().isEmpty()) {
            editText.error = message
        } else {
            editText.error = null
        }
    }

    fun scrollToView(scrollViewParent: NestedScrollView, view: View) {
        // Get deepChild Offset
        val childOffset = Point()
        getDeepChildOffset(scrollViewParent, view.parent, view, childOffset)
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y)
    }

    fun getDeepChildOffset(
        mainParent: ViewGroup,
        parent: ViewParent,
        child: View,
        accumulatedOffset: Point
    ) {
        val parentGroup = parent as ViewGroup
        accumulatedOffset.x += child.left
        accumulatedOffset.y += child.top
        if (parentGroup == mainParent) {
            return
        }
        getDeepChildOffset(mainParent, parentGroup.parent, parentGroup, accumulatedOffset)
    }

    fun showKeyboard(activity: Activity) {
        val mContext = activity.applicationContext
        val inputMethodManager = mContext
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    fun setTint(drawable: Drawable, color: Int): Drawable {
        val wrappedDrawable = DrawableCompat.wrap(drawable)
        DrawableCompat.setTint(wrappedDrawable, color)
        return wrappedDrawable
    }

    fun makeBadge(count: Int, imageView: AppCompatImageView, textView: TextView) {
        if (count > 0) {
            imageView.visibility = View.VISIBLE
            textView.visibility = View.VISIBLE
            textView.text = count.toString()
        } else {
            imageView.visibility = View.GONE
            textView.visibility = View.GONE
        }
    }

    fun getBytes(bitmap: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        return stream.toByteArray()
    }

    fun snackBarMessage(view: View, message: String, time: Int): Snackbar {
        val snackbar = Snackbar.make(view, message, time)
        initSnackbarView(snackbar.view, android.R.color.white, android.R.color.white)
        return snackbar
    }

    fun snackBarMessage(view: View, textColor: Int, message: String, time: Int): Snackbar {
        val snackbar = Snackbar.make(view, message, time)
        initSnackbarView(snackbar.view, textColor, android.R.color.white)
        return snackbar
    }

    fun loadingSnackBar(view: View, message: String): Snackbar {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
        initSnackbarView(snackbar.view, android.R.color.holo_red_light, android.R.color.white)
        return snackbar
    }

    fun snackBarWithAction(
        view: View,
        message: Int,
        action: Int,
        mOnClickListener: View.OnClickListener
    ): Snackbar {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(action, mOnClickListener)
        snackbar.setActionTextColor(Color.RED)
        initSnackbarView(snackbar.view, android.R.color.white, android.R.color.holo_red_light)
        return snackbar
    }

    fun snackbarMessageWithIcon(view: View, message: String, drawable: Int): Snackbar {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        val snackbarview = snackbar.view
        val textView =
            snackbarview.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.compoundDrawablePadding = 30
        textView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        snackbarview.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color80DarkGray))
        textView.gravity = Gravity.CENTER
        val getDrawable = ContextCompat.getDrawable(mContext, drawable)
        getDrawable!!.setColorFilter(
            ContextCompat.getColor(mContext, R.color.colorWhiteDirty),
            PorterDuff.Mode.SRC_ATOP
        )
        textView.setCompoundDrawablesWithIntrinsicBounds(getDrawable, null, null, null)
        return snackbar
    }

    //    public Snackbar snackbarMessageValidation(View view, String message) {
    //        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
    //        View snackbarview = snackbar.getView();
    //        TextView textView = (TextView) snackbarview.findViewById(android.support.design.R.id.snackbar_text);
    //        textView.setCompoundDrawablePadding(30);
    //        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
    //        snackbarview.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorErrorColor));
    //        textView.setGravity(Gravity.CENTER);
    //        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error_outline_white_24dp, 0, 0, 0);
    //        return snackbar;
    //    }

    private fun initSnackbarView(view: View, textColor: Int, actionColor: Int) {
        val textView =
            view.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        val button = view.findViewById<View>(android.support.design.R.id.snackbar_action) as Button

        textView.setTextColor(ContextCompat.getColor(mContext, textColor))
        textView.maxLines = 3

        button.setTextColor(ContextCompat.getColor(mContext, actionColor))

        view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray))
    }

    @SuppressLint("SimpleDateFormat")
    fun showDatePicker(
        activity: Activity,
        min_date: Long,
        max_date: Long,
        calendar: Calendar,
        callback: DatePickerDialog.OnDateSetListener
    ): DatePickerDialog {
        val fromDatePickerDialog = DatePickerDialog(
            activity,
            callback,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog.updateDate(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog.datePicker.minDate = min_date
        fromDatePickerDialog.datePicker.maxDate = max_date
        return fromDatePickerDialog
    }

    @SuppressLint("SimpleDateFormat")
    fun showDatePicker(
        activity: Activity,
        editText: EditText,
        format: String,
        min_date: Long,
        max_date: Long,
        calendar: Calendar
    ) {
        val dateFormatter = SimpleDateFormat(format)

        val fromDatePickerDialog = DatePickerDialog(
            activity,
            DatePickerDialog.OnDateSetListener { datePicker, year1, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year1, monthOfYear, dayOfMonth)

                editText.setText(dateFormatter.format(newDate.time))
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        fromDatePickerDialog.updateDate(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog.datePicker.minDate = min_date
        fromDatePickerDialog.datePicker.maxDate = max_date
        fromDatePickerDialog.show()
    }

    @SuppressLint("SimpleDateFormat")
    fun showDatePicker(
        format: String,
        min_date: Long,
        max_date: Long,
        calendar: Calendar,
        onDateSetListener: DatePickerDialog.OnDateSetListener
    ) {
        val dateFormatter = SimpleDateFormat(format)
        val fromDatePickerDialog = DatePickerDialog(
            mContext,
            onDateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog.updateDate(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        fromDatePickerDialog.datePicker.minDate = min_date
        fromDatePickerDialog.datePicker.maxDate = max_date
        fromDatePickerDialog.show()
    }

    fun getCurrentMonth(): Int {
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.MONTH)
    }

    fun getDay(): String {
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("EEEE")
        return simpleDateFormat.format(calendar.time)
    }

    fun getDateDay(): String {
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("dd")
        return simpleDateFormat.format(calendar.time)
    }

    fun getMonth(): String {
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("MMMM")
        return simpleDateFormat.format(calendar.time)
    }

    fun getCurrentYear(): Int {
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.YEAR)
    }

    fun getCurrentDate(): Long {
        return System.currentTimeMillis() - 1000
    }

    fun getCurrentDateAdvance30mins(): Long {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.MINUTE, 30)
        return calendar.timeInMillis
    }

    fun getYearInMillis(year: Int): Long {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        return calendar.timeInMillis
    }

    fun getScreenWidth(activity: Activity): Int {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.x
    }

    fun getScreenHeight(activity: Activity): Int {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        return size.y
    }

    fun setToolbarMenu(
        menuItem: MenuItem,
        layoutId: Int,
        id: Int,
        onClickListener: View.OnClickListener
    ) {
        menuItem.setActionView(layoutId)
        menuItem.isVisible = true
        val view = menuItem.actionView.findViewById<View>(id)
        view.setOnClickListener(onClickListener)
    }

    fun isHaveValues(vararg editTexts: EditText): Boolean {
        for (editText in editTexts) {
            if (TextUtils.isEmpty(editText.text)) {
                Log.e("isHaveValues", "isEmpty")
                return true
            }
        }
        Log.e("isHaveValues", "NOT EMPTY")
        return false
    }

    fun getTextNotNull(editText: EditText): String {
        return if (TextUtils.isEmpty(editText.text)) "" else editText.text.toString()
    }

    fun setGreyscale(v: View, greyscale: Boolean): View {
        if (greyscale) {
            // Create a paint object with 0 saturation (black and white)
            val cm = ColorMatrix()
            cm.setSaturation(0f)

            val greyscalePaint = Paint()
            greyscalePaint.colorFilter = ColorMatrixColorFilter(cm)
            // Create a hardware layer with the greyscale paint
            v.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint)
        } else {
            // Remove the hardware layer
            v.setLayerType(View.LAYER_TYPE_NONE, null)
        }
        return v
    }

    fun getCorporateOrganizationInitial(organization_name: String): String {
        val roleInitial = organization_name.split(" ")
        return if (roleInitial.size > 1) {
            roleInitial[0][0].toString() + roleInitial[1][0].toString()
        } else {
            roleInitial[0][0].toString()
        }
    }


    @SuppressLint("SimpleDateFormat")
    fun getDate(dateString: String?, givenFormat: String): Date {
        return SimpleDateFormat(givenFormat).parse(dateString)
    }

    @SuppressLint("SimpleDateFormat")
    fun getCalendar(dateString: String?, givenFormat: String): Calendar {
        val calendarEndDate = Calendar.getInstance()
        val date = SimpleDateFormat(givenFormat).parse(dateString)
        calendarEndDate.time = date
        return calendarEndDate
    }

    @SuppressLint("SimpleDateFormat")
    fun getFormattedDate(dateString: String?, givenFormat: String, desireFormat: String): String {
        return if (dateString != null) {
            val date = SimpleDateFormat(givenFormat).parse(dateString)
            val simpleDateFormat = SimpleDateFormat(desireFormat)
            simpleDateFormat.format(date)
        } else {
            Constant.EMPTY
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getFormattedDate(date: Long?, desireFormat: String): String {
        return if (date != null) {
            val simpleDateFormat = SimpleDateFormat(desireFormat)
            simpleDateFormat.format(date)
        } else {
            ""
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun getDate(dateString: String?): String {
        var format = SimpleDateFormat("yyyy-MM-dd")
        val date1 = format.parse(dateString)
        val date = format.format(date1)

        if (date.endsWith("01") && !date.endsWith("11"))
            format = SimpleDateFormat("d'st' MMMM, yyyy")
        else if (date.endsWith("02") && !date.endsWith("12"))
            format = SimpleDateFormat("d'nd' MMMM, yyyy")
        else if (date.endsWith("03") && !date.endsWith("13"))
            format = SimpleDateFormat("d'rd' MMMM, yyyy")
        else
            format = SimpleDateFormat("d'th' MMMM, yyyy")

        return format.format(date1)
    }

    @SuppressLint("SimpleDateFormat")
    fun getFormattedDates(dateString: String?, desireFormat: String): String? {
        formatStrings.forEach {
            try {
                val date = SimpleDateFormat(it).parse(dateString)
                val simpleDateFormat = SimpleDateFormat(desireFormat)
                return simpleDateFormat.format(date)
            } catch (e: ParseException) {

            }
        }
        return Constant.EMPTY
    }

    fun getStringOrEmpty(string: String?): String? {
        return if (string != null && string != "") {
            string
        } else {
            Constant.EMPTY
        }
    }

    fun getStringEmpty(string: String?): String? {
        return string ?: ""
    }

    fun getStringOrDefaultString(string: String?, default: String?): String? {
        return if (string != null && string != "") {
            string
        } else {
            default
        }
    }

    fun getAccountStatus(status: String?): String? {
        return if (status == "A") {
            "Active"
        } else {
            "Inactive"
        }
    }

    fun getAccountNumberFormat(accountNumber: String?): String {
        if (accountNumber == null) return Constant.EMPTY
        val accountNumberStringBuilder = StringBuilder()
        accountNumber.forEachIndexed { index, c ->
            if ((index + 1) % 4 == 0) {
                accountNumberStringBuilder.append("$c ")
            } else {
                accountNumberStringBuilder.append(c)
            }
        }
        return accountNumberStringBuilder.toString()
    }

    fun getOTPCode(message: String): String {
        val p = Pattern.compile("(\\d{6})")
        val m = p.matcher(message)
        return if (m.find()) {
            m.group(0)
        } else ""
    }

    fun makeMeasureSpec(dimension: Int): Int {
        return if (dimension > 0) {
            View.MeasureSpec.makeMeasureSpec(dimension, View.MeasureSpec.EXACTLY)
        } else {
            View.MeasureSpec.UNSPECIFIED
        }
    }

    fun loadJSONFromAsset(context: Context, file: String): String {
        val json: String?
        try {
            val inputStream = context.assets.open("json/$file.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer)
        } catch (ex: IOException) {
            Timber.e(ex, "loadJSONFromAsset")
            return ""
        }

        return json
    }


    fun setInputType(editText: EditText?, inputTypeEnum: String?) {
        when (inputTypeEnum) {
            "receiveEmail address" -> editText?.inputType =
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or
                        InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
            "numeric_padding",
            "numeric",
            "number" -> editText?.inputType = InputType.TYPE_CLASS_NUMBER
            "multiline" -> editText?.inputType =
                InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            else -> editText?.inputType = InputType.TYPE_CLASS_TEXT
        }
    }

    fun getFileText(activity: Activity, txtFile: String): String {
        val text: String?
        try {
            val inputStream = activity.assets.open("txt/$txtFile.txt")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            text = String(buffer)
        } catch (ex: IOException) {
            Timber.e(ex, "getFileText")
            return ""
        }
        return text
    }

    fun getAge(birthDate: String?): String {
        if (birthDate == null) return "N/A"
        val birthDateExploded = birthDate.split("/")
        val day = birthDateExploded[0].toInt()
        val month = birthDateExploded[1].toInt()
        val year = birthDateExploded[2].toInt()
        val cal = Calendar.getInstance()

        val y = cal.get(Calendar.YEAR)
        val m = cal.get(Calendar.MONTH)
        val d = cal.get(Calendar.DAY_OF_MONTH)
        cal.set(year, month, day)
        var a = y - cal.get(Calendar.YEAR)
        if ((m < cal.get(Calendar.MONTH)) ||
            ((m == cal.get(Calendar.MONTH)) &&
                    (d < cal.get(Calendar.DAY_OF_MONTH)))
        ) {
            --a
        }
        if (a < 0)
            throw IllegalArgumentException("Age < 0")
        return a.toString()
    }

    fun formatWithTwoDecimalPlaces(value: Double?): String {
        try {
            val formatter = DecimalFormat("#,##0.00")
            return formatter.format(value)
        } catch (e: NullPointerException) {
            Timber.e(e, "formatWithTwoDecimalPlaces")
        }
        return "-"
    }


}
