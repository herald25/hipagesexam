package com.sample.hipagesexam.app

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.os.Bundle
import android.support.multidex.MultiDex
import android.support.v4.app.Fragment
import com.sample.hipagesexam.BuildConfig
import com.sample.hipagesexam.app.common.logging.ErrorReportingTree
import com.sample.hipagesexam.app.injection.component.DaggerAppComponent
import dagger.android.*
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import javax.inject.Inject


open class App : Application(),
    HasActivityInjector,
    HasSupportFragmentInjector,
    Application.ActivityLifecycleCallbacks {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
        DaggerAppComponent.builder().application(this).build().inject(this)
        initialize()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        try {
            MultiDex.install(this)
        } catch (multiDexException: RuntimeException) {
            Timber.e(multiDexException, "attachBaseContext")
        }
    }

    private fun initialize() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())

            // https://developer.android.com/reference/android/os/StrictMode.html
            // StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build())
            // StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build())
            // StrictMode.enableDefaults()


        } else {
            Timber.plant(ErrorReportingTree())
            // https://medium.com/fuzz/getting-the-most-out-of-crashlytics-380afb703876
            // Crashlytics.setString("git_sha", BuildConfig.GIT_SHA)
        }
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityInjector
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun onActivityPaused(p0: Activity?) {
        isActive = false
    }

    override fun onActivityResumed(p0: Activity?) {
        isActive = true
    }

    override fun onActivityStarted(p0: Activity?) {

    }

    override fun onActivityDestroyed(p0: Activity?) {

    }

    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {

    }

    override fun onActivityStopped(p0: Activity?) {

    }

    override fun onActivityCreated(p0: Activity?, p1: Bundle?) {

    }

    companion object {
        var isActive = false
        fun isActivityVisible() = isActive
    }

}