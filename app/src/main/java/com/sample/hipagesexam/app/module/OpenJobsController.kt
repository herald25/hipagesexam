package com.sample.hipagesexam.app.module

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.*
import com.sample.hipagesexam.R
import com.sample.hipagesexam.app.common.callback.EpoxyAdapterCallback
import com.sample.hipagesexam.app.util.ViewUtil
import com.sample.hipagesexam.jobs.model.ConnectedBusinesses
import com.sample.hipagesexam.jobs.model.Jobs
import com.sample.hipagesexam.jobs.model.JobsDto
import kotlinx.android.synthetic.main.item_job.view.*


class JobsController
constructor(
    private val context: Context,
    private val viewUtil: ViewUtil
) : TypedEpoxyController<JobsDto>() {

    private lateinit var epoxyAdapterCallback: EpoxyAdapterCallback

    init {
        isDebugLoggingEnabled = true
    }

    override fun buildModels(data: JobsDto) {
        data.jobs.forEachIndexed { index, job ->
            jobItem {
                id(job.jobId)
                jobs(job)
                position(index)
                context(context)
                viewUtil(viewUtil)
                callbacks(epoxyAdapterCallback)
            }
        }
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        // Best practice is to throw in debug so you are aware of any issues that Epoxy notices.
        // Otherwise Epoxy does its best to swallow these exceptions and continue gracefully
        throw exception
    }

    fun setEpoxyAdapterCallback(epoxyAdapterCallback: EpoxyAdapterCallback) {
        this.epoxyAdapterCallback = epoxyAdapterCallback
    }

}

@EpoxyModelClass(layout = R.layout.item_job)
abstract class JobItemModel : EpoxyModelWithHolder<JobItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var context: Context

    @EpoxyAttribute
    lateinit var viewUtil: ViewUtil

    @EpoxyAttribute
    lateinit var jobs: Jobs

    @EpoxyAttribute
    lateinit var callbacks: EpoxyAdapterCallback

    @EpoxyAttribute
    var position: Int = 0

    private lateinit var controller: BusinessController

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.textViewCategory.text = jobs.category
        holder.textViewPostedDate.text =
            String.format(context.getString(R.string.param_posted_date), viewUtil.getDate(jobs.postedDate))
        holder.textViewHiredCount.text =
            if (jobs.connectedBusinesses?.size == null)
                context.getString(R.string.msg_connecting_businesses)
            else
                String.format(
                    context.getString(R.string.param_hired_businesses),
                    jobs.connectedBusinesses?.size ?: "0"
                )
        controller = BusinessController(context, viewUtil)
        holder.recyclerViewBusinesses.layoutManager = GridLayoutManager(context, 4)
        holder.recyclerViewBusinesses.setController(controller)
        updateController(jobs.connectedBusinesses ?: mutableListOf())
        holder.imageViewMore.setOnClickListener {
            callbacks.onClickItem(holder.imageViewMore, jobs, position)
        }
    }

    private fun updateController(data: MutableList<ConnectedBusinesses>) {
        controller.setData(data)
    }

    class Holder : EpoxyHolder() {
        lateinit var cardView: CardView
        lateinit var textViewCategory: TextView
        lateinit var textViewPostedDate: TextView
        lateinit var textViewHiredCount: TextView
        lateinit var recyclerViewBusinesses: EpoxyRecyclerView
        lateinit var imageViewMore: ImageView

        override fun bindView(itemView: View) {
            cardView = itemView.cardView
            textViewCategory = itemView.textViewCategory
            textViewPostedDate = itemView.textViewPostedDate
            textViewHiredCount = itemView.textViewHiredCount
            recyclerViewBusinesses = itemView.recyclerViewBusinesses
            imageViewMore = itemView.imageViewMore
        }
    }
}
