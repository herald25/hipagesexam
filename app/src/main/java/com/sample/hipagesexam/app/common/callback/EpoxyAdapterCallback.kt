package com.sample.hipagesexam.app.common.callback

import android.view.View

interface EpoxyAdapterCallback {
    fun onClickItem(view: View, model: Any, position: Int)
}