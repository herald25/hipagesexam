package com.sample.hipagesexam.app.injection.component

import android.app.Application
import com.sample.hipagesexam.app.App
import com.sample.hipagesexam.app.injection.module.*
import com.sample.hipagesexam.app.injection.module.EpoxyControllerModule
import com.sample.hipagesexam.app.injection.module.FragmentBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        DataModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        EpoxyControllerModule::class,
        ActivityBindingModule::class,
        FragmentBindingModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
