package com.sample.hipagesexam.app.module

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.sample.hipagesexam.R
import com.sample.hipagesexam.app.base.BaseActivity
import com.sample.hipagesexam.app.base.BaseFragment
import com.sample.hipagesexam.app.common.callback.EpoxyAdapterCallback
import com.sample.hipagesexam.jobs.model.JobsDto
import kotlinx.android.synthetic.main.fragment_jobs.*
import timber.log.Timber
import javax.inject.Inject


class OpenJobFragment : BaseFragment(), EpoxyAdapterCallback, PopupMenu.OnMenuItemClickListener {

    @Inject
    lateinit var jobsController: JobsController

    private lateinit var mainViewModel: MainViewModel

    override fun layoutId() = R.layout.fragment_jobs

    override fun onViewsBound() {
        super.onViewsBound()
        initRecyclerView()
    }

    override fun onViewModelBound() {
        super.onViewModelBound()
        initViewModel()
    }


    override fun onInitializeListener() {
        super.onInitializeListener()
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent)
        swipeRefreshLayout.setOnRefreshListener {
            getOpenJobs()
        }
    }

    override fun onClickItem(view: View, model: Any, position: Int) {
        val popup = PopupMenu(activity!!, view)
        popup.setOnMenuItemClickListener(this)
        popup.inflate(R.menu.menu_main)
        popup.show()
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_close -> {
                Toast.makeText(activity, "Close", Toast.LENGTH_SHORT).show()
                true
            }
            else -> false
        }
    }

    private fun initViewModel() {
        mainViewModel =
            ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
        mainViewModel.state.observe(this, Observer {
            when (it) {
                is ShowMainLoading -> {
                    (activity as BaseActivity).showLoading(
                        viewLoadingState,
                        swipeRefreshLayout,
                        recyclerView,
                        textViewState
                    )
                }
                is ShowMainDismissLoading -> {
                    (activity as BaseActivity).dismissLoading(
                        viewLoadingState,
                        swipeRefreshLayout,
                        recyclerView
                    )
                }
                is ShowMainGetOpenJobs -> {
                    Timber.d("data: " + it.data)
                    updateController(it.data)
                }
            }
        })
        getOpenJobs()
    }

    private fun initRecyclerView() {
        jobsController.setEpoxyAdapterCallback(this)
        recyclerView.setController(jobsController)
    }

    private fun updateController(data: JobsDto) {
        jobsController.setData(data)
    }

    private fun getOpenJobs() {
        mainViewModel.getOpenJobs()
    }

    companion object {
        fun newInstance(): OpenJobFragment {
            val fragment =
                OpenJobFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }
}
