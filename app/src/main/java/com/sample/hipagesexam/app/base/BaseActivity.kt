package com.sample.hipagesexam.app.base

import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.content.res.Resources
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.AppBarLayout
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import com.sample.hipagesexam.R
import com.sample.hipagesexam.app.common.navigation.Navigator
import com.sample.hipagesexam.app.util.ViewUtil
import com.sample.hipagesexam.common.retrofit.executor.SchedulerProvider
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var viewUtil: ViewUtil

    val disposables = CompositeDisposable()

    protected open fun beforeLayout(savedInstanceState: Bundle?) {}
    protected open fun afterLayout(savedInstanceState: Bundle?) {}
    protected open fun onViewsBound() {}

    protected open fun onViewModelBound() {}

    protected open fun onInitializeListener() {}

    @LayoutRes
    protected abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        beforeLayout(savedInstanceState)
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        afterLayout(savedInstanceState)
        onViewModelBound()
        onViewsBound()
        onInitializeListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
        disposables.dispose()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        this.intent = intent
    }

    fun initTransparency() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
//        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
//            viewUtil.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true)
//        }
//        if (Build.VERSION.SDK_INT >= 19) {
//            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//        }
//        if (Build.VERSION.SDK_INT >= 21) {
//            viewUtil.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
//            window.statusBarColor = Color.TRANSPARENT
//        }
    }

    fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    fun initToolbar(toolbar: Toolbar?) {
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    fun showToolbarIcon(isShown: Boolean, textView: TextView) {
        if (isShown) {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        } else {
            val viewParams = textView.layoutParams as ViewGroup.MarginLayoutParams
            viewParams.setMargins(
                resources.getDimensionPixelSize(R.dimen.content_spacing),
                viewParams.topMargin,
                viewParams.rightMargin,
                viewParams.bottomMargin
            )
            supportActionBar!!.setDisplayShowHomeEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        }
    }

    fun getStatusBarHeight(): Int {
        return if (Build.VERSION.SDK_INT >= 21) {
            var result = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = resources.getDimensionPixelSize(resourceId)
            }
            return result
        } else 0
    }

    fun getNavHeight(): Int {
        val resources = resources
        return if (hasNavBar(resources)) {
            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
            return if (resourceId > 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
        } else 0
    }

    fun hasNavBar(resources: Resources): Boolean {
        val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && resources.getBoolean(id)
    }

    fun setToolbarTitle(tvToolbar: TextView?, title: String) {
        tvToolbar?.text = title
    }

    fun setToolbarTitle(
        textViewTitle: TextView?,
        textViewOrganizationName: TextView?,
        title: String,
        orgName: String
    ) {
        textViewTitle?.text = title
        textViewOrganizationName?.text = orgName
    }

    fun clearToolbarScrollFlags(toolbar: Toolbar?) {
        val params = toolbar?.layoutParams as AppBarLayout.LayoutParams
        params.scrollFlags = 0

    }

    fun addToolbarScrollFlags(toolbar: Toolbar?) {
        val params = toolbar?.layoutParams as AppBarLayout.LayoutParams
        params.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or
                AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
    }

    fun setDrawableBackButton(resource: Int) {
        val arrowDrawable = ContextCompat.getDrawable(this, resource)

        arrowDrawable?.setColorFilter(
            ContextCompat.getColor(this, R.color.colorWhiteDirty), PorterDuff.Mode.SRC_ATOP
        )
        supportActionBar?.setHomeAsUpIndicator(arrowDrawable)
    }

    fun showLoading(
        viewLoadingState: View,
        swipeRefreshLayout: SwipeRefreshLayout,
        recyclerView: RecyclerView,
        textView: TextView
    ) {
        if (!swipeRefreshLayout.isRefreshing) {
            viewLoadingState.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
            textView.visibility = View.GONE
        }
        if (viewLoadingState.visibility == View.VISIBLE) {
            swipeRefreshLayout.isEnabled = true
        }
    }

    fun dismissLoading(
        viewLoadingState: View,
        swipeRefreshLayout: SwipeRefreshLayout,
        recyclerView: RecyclerView
    ) {
        if (!swipeRefreshLayout.isRefreshing) {
            viewLoadingState.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        } else swipeRefreshLayout.isRefreshing = false
        swipeRefreshLayout.isEnabled = true
    }

}
