package com.sample.hipagesexam.app.module

import android.content.Context
import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.*
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sample.hipagesexam.R
import com.sample.hipagesexam.app.common.glide.GlideApp
import com.sample.hipagesexam.app.util.ViewUtil
import com.sample.hipagesexam.jobs.model.ConnectedBusinesses
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_business.view.*


class BusinessController
constructor(
    private val context: Context,
    private val viewUtil: ViewUtil
) : TypedEpoxyController<MutableList<ConnectedBusinesses>>() {

    init {
        isDebugLoggingEnabled = true
    }

    override fun buildModels(data: MutableList<ConnectedBusinesses>) {
        data.forEachIndexed { index, connectedBusinesses ->
            businessItem {
                id(connectedBusinesses.businessId)
                connectedBusinesses(connectedBusinesses)
                position(index)
                context(context)
                viewUtil(viewUtil)
            }
        }
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        // Best practice is to throw in debug so you are aware of any issues that Epoxy notices.
        // Otherwise Epoxy does its best to swallow these exceptions and continue gracefully
        throw exception
    }

}

@EpoxyModelClass(layout = R.layout.item_business)
abstract class BusinessItemModel : EpoxyModelWithHolder<BusinessItemModel.Holder>() {

    @EpoxyAttribute
    lateinit var context: Context

    @EpoxyAttribute
    lateinit var viewUtil: ViewUtil

    @EpoxyAttribute
    lateinit var connectedBusinesses: ConnectedBusinesses

    @EpoxyAttribute
    var position: Int = 0

    override fun bind(holder: Holder) {
        super.bind(holder)
        GlideApp.with(context)
            .load(connectedBusinesses.thumbnail)
            .centerCrop()
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .into(holder.circularImageViewBusiness)
        if (connectedBusinesses.isHired) {
            holder.textViewBusinessStatus.visibility = View.VISIBLE
            holder.textViewBusinessStatus.text = context.getString(R.string.title_hired)
        } else {
            holder.textViewBusinessStatus.visibility = View.GONE
        }
    }

    class Holder : EpoxyHolder() {
        lateinit var circularImageViewBusiness: CircleImageView
        lateinit var textViewBusinessStatus: TextView

        override fun bindView(itemView: View) {
            circularImageViewBusiness = itemView.circularImageViewBusiness
            textViewBusinessStatus = itemView.textViewBusinessStatus
        }
    }
}
