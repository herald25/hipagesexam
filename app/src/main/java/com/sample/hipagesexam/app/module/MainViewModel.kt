package com.sample.hipagesexam.app.module

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.sample.hipagesexam.app.base.BaseViewModel
import com.sample.hipagesexam.app.common.connectivity.ConnectivityChecker
import com.sample.hipagesexam.common.retrofit.executor.SchedulerProvider
import com.sample.hipagesexam.jobs.JobsGateway
import com.sample.hipagesexam.jobs.model.JobsDto
import io.reactivex.rxkotlin.addTo
import timber.log.Timber
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val connectivityChecker: ConnectivityChecker,
    private val jobsGateway: JobsGateway
) : BaseViewModel() {

    private val _actvitiesState = MutableLiveData<MainState>()

    val state: LiveData<MainState> get() = _actvitiesState

    fun getOpenJobs() {
        connectivityChecker
            .isNetworkConnectedSingle()
            .flatMap { jobsGateway.getOpenJobs() }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doOnSubscribe { _actvitiesState.value = ShowMainLoading }
            .doFinally { _actvitiesState.value = ShowMainDismissLoading }
            .subscribe(
                {
                    _actvitiesState.value = ShowMainGetOpenJobs(it)
                }, {
                    Timber.e(it, "getOpenJobs Failed")
                    _actvitiesState.value = ShowMainError(it)
                })
            .addTo(disposables)
    }

}

sealed class MainState

object ShowMainLoading : MainState()

object ShowMainDismissLoading : MainState()

data class ShowMainGetOpenJobs(val data: JobsDto) : MainState()

data class ShowMainError(val throwable: Throwable) : MainState()