package com.sample.hipagesexam.app.injection.module

import android.content.Context
import com.sample.hipagesexam.app.module.JobsController
import com.sample.hipagesexam.app.util.ViewUtil
import dagger.Module
import dagger.Provides

@Module
class EpoxyControllerModule {

    @Provides
    internal fun mainController(
        context: Context,
        viewUtil: ViewUtil
    ) = JobsController(context, viewUtil)
}