package com.sample.hipagesexam.app.module

import android.os.Bundle
import android.support.v4.view.ViewPager.OnPageChangeListener
import android.view.MenuItem
import com.sample.hipagesexam.R
import com.sample.hipagesexam.app.base.BaseActivity
import com.sample.hipagesexam.app.common.widget.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.widget_transparent_toolbar.*


class MainActivity : BaseActivity() {

    private var adapter: ViewPagerAdapter? = null

    override fun layoutId(): Int = R.layout.activity_main

    override fun afterLayout(savedInstanceState: Bundle?) {
        super.afterLayout(savedInstanceState)
        initToolbar(toolbar)
        setDrawableBackButton(R.drawable.ic_menu_black_24dp)
        setToolbarTitle(
            tvToolbar,
            getString(R.string.title_hipages)
        )
    }

    override fun onViewsBound() {
        super.onViewsBound()
        setupViewPager()
    }

    override fun onInitializeListener() {
        super.onInitializeListener()
        viewPagerJobs.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
            }
        })
    }

    private fun setupViewPager() {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter?.addFragment(
            OpenJobFragment.newInstance(),
            FRAGMENT_OPEN_JOBS
        )
        adapter?.addFragment(
            OpenJobFragment.newInstance(),
            FRAGMENT_CLOSED_JOBS
        )
        viewPagerJobs.offscreenPageLimit = 1
        viewPagerJobs.currentItem = 0
        viewPagerJobs.adapter = adapter
        tabLayoutJobs.setupWithViewPager(viewPagerJobs, false)
    }

    companion object {
        const val FRAGMENT_OPEN_JOBS = "Open Jobs"
        const val FRAGMENT_CLOSED_JOBS = "Closed Jobs"
    }

}


