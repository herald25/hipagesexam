package com.sample.hipagesexam.app.injection.module

import android.content.Context
import com.sample.hipagesexam.common.retrofit.executor.ResponseProvider
import com.sample.hipagesexam.jobs.JobsDataGateway
import com.sample.hipagesexam.jobs.JobsGateway
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class DataModule {

    @Provides
    @Singleton
    fun jobsGateway(
        context: Context,
        retrofit: Retrofit,
        responseProvider: ResponseProvider
    ): JobsGateway = JobsDataGateway(
        context,
        retrofit,
        responseProvider
    )

}