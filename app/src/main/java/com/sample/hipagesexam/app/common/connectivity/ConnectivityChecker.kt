package com.sample.hipagesexam.app.common.connectivity

import android.net.ConnectivityManager
import io.reactivex.Single


/**
 * Class which checks if internet connection is present on given device.
 * Since sometimes there is no internet connection present(bad cellular reception, wifi at the airport - sign up needed),
 * class performs pinging of Google servers.
 */
class ConnectivityChecker(private val connectivityManager: ConnectivityManager) {

    private fun hasConnection(): Boolean {
        return (connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected)
    }

    fun isNetworkConnectedSingle(): Single<Boolean> {
        return Single.fromCallable { hasConnection() }
    }
    
}