package com.sample.hipagesexam.app.injection.module

import android.content.Context
import com.jakewharton.retrofit2.converter.kotlinx.serialization.serializationConverterFactory
import com.sample.hipagesexam.BuildConfig
import com.sample.hipagesexam.R
import com.sample.hipagesexam.common.retrofit.interceptor.RequestInterceptor
import dagger.Module
import dagger.Provides
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


@Module
class NetworkModule {

    @Provides
    @Named("prodOkhttpClient")
    @Singleton
    internal fun provideOkHttpClient(context: Context): OkHttpClient {

        val client = OkHttpClient.Builder()
        client.addInterceptor(RequestInterceptor(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET))
        //client.addInterceptor(ResponseErrorInterceptor(context, viewUtil))
        //client.addInterceptor(ConnectivityInterceptor(connectivityChecker.isNetworkConnectedObservable()))
        client.readTimeout(context.resources.getInteger(R.integer.time_api_request_time_out).toLong(), TimeUnit.SECONDS)
        client.connectTimeout(
            context.resources.getInteger(R.integer.time_api_request_time_out).toLong(),
            TimeUnit.SECONDS
        )
        client.writeTimeout(
            context.resources.getInteger(R.integer.time_api_request_time_out).toLong(),
            TimeUnit.SECONDS
        )

        if (BuildConfig.DEBUG) {
            val logInterceptor = HttpLoggingInterceptor()
            logInterceptor.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(logInterceptor)
        }

        return client.build()
    }

    @Provides
    @Singleton
    internal fun provideRestAdapter(
        @Named("prodOkhttpClient")
        okHttpClient: OkHttpClient
    ): Retrofit {
        val contentType = MediaType.get("application/json")!!
        val json = Json(strictMode = false, indented = true)
        return Retrofit.Builder()
            .baseUrl(BuildConfig.HOST)
            .addConverterFactory(serializationConverterFactory(contentType, json))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Named("unsafeOkhttpClient")
    @Singleton
    internal fun provideUnsafeOkhttpClient(context: Context): OkHttpClient {

        /* Trust anything*/
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return emptyArray()
            }

            @Throws(CertificateException::class)
            override fun checkClientTrusted(
                chain: Array<java.security.cert.X509Certificate>,
                authType: String
            ) {
                //checkClientTrusted
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(
                chain: Array<java.security.cert.X509Certificate>,
                authType: String
            ) {
                //checkServerTrusted
            }
        })

        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        val sslSocketFactory = sslContext.socketFactory

        val client = OkHttpClient.Builder()
        client.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        client.hostnameVerifier { _, _ -> true }

        /* Rest of config*/
        client.addInterceptor(RequestInterceptor(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET))
        client.readTimeout(context.resources.getInteger(R.integer.time_api_request_time_out).toLong(), TimeUnit.SECONDS)
        client.connectTimeout(
            context.resources.getInteger(R.integer.time_api_request_time_out).toLong(),
            TimeUnit.SECONDS
        )
        client.writeTimeout(
            context.resources.getInteger(R.integer.time_api_request_time_out).toLong(),
            TimeUnit.SECONDS
        )

        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.NONE
        client.addInterceptor(logInterceptor)

        if (!BuildConfig.DEBUG) {
            throw RuntimeException("You fool. Do not use this in production!!!")
        }

        return client.build()
    }
}
