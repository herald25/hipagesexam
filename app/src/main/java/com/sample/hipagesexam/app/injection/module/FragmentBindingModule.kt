package com.sample.hipagesexam.app.injection.module

import com.sample.hipagesexam.app.module.OpenJobFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun openJobFragment(): OpenJobFragment

}
