package com.sample.hipagesexam.app.common.navigation

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class Navigator {

    fun navigate(
        source: AppCompatActivity,
        target: Class<out AppCompatActivity>,
        bundle: Bundle? = null,
        isClear: Boolean
    ) {
        val intent = Intent(source, target)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        bundle?.let { intent.putExtras(it) }
        source.startActivity(intent)
        if (isClear) source.finish()
    }

    fun navigateClearUpStack(
        source: AppCompatActivity,
        target: Class<out AppCompatActivity>,
        bundle: Bundle? = null,
        isClear: Boolean
    ) {
        val intent = Intent(source, target)
        if (isClear) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        bundle?.let { intent.putExtras(it) }
        source.startActivity(intent)
    }

    fun navigateClearStacks(
        source: AppCompatActivity,
        target: Class<out AppCompatActivity>,
        bundle: Bundle? = null
    ) {
        val intent = Intent(source, target)
        bundle?.let { intent.putExtras(it) }
        source.startActivity(intent)
        source.finishAffinity()
    }


    fun navigateClearStacks(
        source: AppCompatActivity,
        target: Class<out AppCompatActivity>,
        bundle: Bundle? = null,
        isClear: Boolean
    ) {
        val intent = Intent(source, target)
        if (isClear) {
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        bundle?.let { intent.putExtras(it) }
        source.startActivity(intent)
        if (isClear) source.finish()
    }

}