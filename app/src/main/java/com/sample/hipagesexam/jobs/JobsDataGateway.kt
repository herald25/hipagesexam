package com.sample.hipagesexam.jobs

import android.content.Context
import com.sample.hipagesexam.common.retrofit.executor.ResponseProvider
import com.sample.hipagesexam.jobs.model.JobsDto
import io.reactivex.Single
import kotlinx.serialization.json.Json
import retrofit2.Retrofit
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class JobsDataGateway
@Inject
constructor(
    private val context: Context,
    retrofit: Retrofit,
    private val responseProvider: ResponseProvider
) : JobsGateway {

    private val jobsApiClient: JobsApiClient = retrofit.create(JobsApiClient::class.java)

    override fun getOpenJobs(): Single<JobsDto> {
        //in the future parse data to the jobsApiClient class but as of now we will use the json file in raw directory
        return Single.fromCallable {
            Json.parse(JobsDto.serializer(), loadJSONFromAsset(context, "jobs"))
        }
    }

    fun loadJSONFromAsset(context: Context, file: String): String {
        val json: String?
        try {
            val inputStream = context.assets.open("$file.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer)
        } catch (ex: IOException) {
            Timber.e(ex, "loadJSONFromAsset")
            return ""
        }

        return json
    }
}


