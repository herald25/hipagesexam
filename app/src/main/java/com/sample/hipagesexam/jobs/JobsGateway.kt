package com.sample.hipagesexam.jobs

import com.sample.hipagesexam.jobs.model.JobsDto
import io.reactivex.Single

interface JobsGateway {

    fun getOpenJobs(): Single<JobsDto>

}
