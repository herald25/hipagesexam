package com.sample.hipagesexam.jobs.model

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class JobsDto(
    @Optional
    @SerialName("jobs")
    var jobs: MutableList<Jobs> = mutableListOf()
)

@Serializable
data class Jobs(
    @Optional
    @SerialName("jobId")
    var jobId: Int? = null,
    @Optional
    @SerialName("category")
    var category: String? = null,
    @Optional
    @SerialName("postedDate")
    var postedDate: String? = null,
    @Optional
    @SerialName("status")
    var status: String? = null,
    @Optional
    @SerialName("connectedBusinesses")
    var connectedBusinesses: MutableList<ConnectedBusinesses>? = null,
    @Optional
    @SerialName("detailsLink")
    var detailsLink: String? = null
)

@Serializable
data class ConnectedBusinesses(
    @Optional
    @SerialName("businessId")
    var businessId: Int? = null,
    @Optional
    @SerialName("thumbnail")
    var thumbnail: String? = null,
    @Optional
    @SerialName("isHired")
    var isHired: Boolean = false
)